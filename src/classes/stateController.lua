-----------------------------------------------------------------------------------------
--  Класс контроллера состояний.
-----------------------------------------------------------------------------------------

local stateController = {}
stateController.__index = stateController

function stateController.new(beginState)
    local instance = {
        state = beginState,
        prevState = nil,
        onUpdate = nil,
    }

    setmetatable(instance, stateController)

    if instance.state ~= nil then
        instance:setState(instance.state)
    end

    return instance
end

function stateController:setState(newState)
    --пробегаюсь по списку состояний и ищу совпадение с newState.
    local isCanTransit = false
    -- print("transitions", #self.state.transitions)
    for i = 1, #self.state.transitions do
        -- print(i, self.state.transitions[i].name)
        if newState.name == self.state.transitions[i].name then isCanTransit = true break end
    end

    if isCanTransit then
        print("Transition from", self.state.name, "to", newState.name)

        self.prevState = self.state
        self.state:exit()
        self:removeUpdate()

        self.state = newState
        self.state:enter()
        if self.state.onUpdate then
            self.onUpdate = self.state.onUpdate
            Runtime:addEventListener("enterFrame", self.onUpdate)
        end
    else
        print("There is no transition in list")
    end

end

function stateController:removeUpdate()
    Runtime:removeEventListener("enterFrame", self.onUpdate)
end

function stateController:destroy()
    self:removeUpdate()
end

return stateController