-----------------------------------------------------------------------------------------
--  Класс анимаций v0.1
--  Оборачивает вывод библиотеки spritesheet.
--  2017
-----------------------------------------------------------------------------------------

require("src.lib.spritesheet")

local animation = {}
animation.__index = animation

function animation.new(sheet, name, x, y, timeScale, loopCount)
    local instance = {
        sheet = sheet,
        name = name,
        initialX = x,
        initialY = y,
        timeScale = timeScale or 0.2,
        loopCount = loopCount or 0,
        obj = display.newSheetAnimation(sheet, name),
        spriteListener = nil,
    }

    setmetatable(instance, animation)
    instance:init()
    return instance
end

function animation:writeName(newState)
    -- print("State name:", self.name)
end

function animation:init()
    self.obj.x = self.initialX
    self.obj.y = self.initialY + 40
    self.obj.timeScale = self.timeScale
    self.obj.anchorY = 1

    self.spriteListener = function(event)
        if (event.phase == "began") then
            -- print("began")
        elseif (event.phase == "ended") then
            if self.loopCount == 0 then
                self.obj:play() --повторяю анимацию
            end
        end
    end
    self.obj:addEventListener("sprite", self.spriteListener)
end

function animation:changeAnimation(name)
    self.obj:setSequence(name)
    self.obj:play()
end

function animation:setTimeScale(timeScale)
    self.obj.timeScale = timeScale
end

function animation:setLoopCount(loopCount)
    self.loopCount = loopCount
end

function animation:play()
    self.obj:play()
end

function animation:remove()
    self.obj:removeEventListener("sprite", self.spriteListener)
end

return animation