-----------------------------------------------------------------------------------------
--  Класс состояния.
-----------------------------------------------------------------------------------------

local baseState = {}
baseState.__index = baseState

function baseState.new(name)
    local instance = {
        name = name,
        transitions = {},
        onEnter = nil,
        onExit = nil,
        onUpdate = nil,
    }

    setmetatable(instance, baseState)
    return instance
end

function baseState:writeName(newState)
    print("State name:", self.name)
end

function baseState:addTransitions(targetState)
    self.transitions[#self.transitions+1] = targetState
end

function baseState:onEnter()
    --override
end

function baseState:onExit()
    --override
end

function baseState:onUpdate()
    --override
end

function baseState:enter()
    print( "State:",  self.name, "enter")
    self:onEnter()
end

function baseState:exit()
    print( "State:",  self.name, "exit")
    self:onExit()
end

return baseState