local spritesheet = {}
spritesheet.__index = spritesheet

function spritesheet.new(image)
    local _spriteSheetInfo = "assets.spritesheets."..image
    local _spriteSheetImage = "assets/spritesheets/"..image..".png"

    local data = {
        spriteSheetInfo = require(_spriteSheetInfo),
        spriteSheetImage = _spriteSheetImage,
        imageSheet = nil
    }

    data.imageSheet = graphics.newImageSheet(data.spriteSheetImage, data.spriteSheetInfo:getSheet())

    return setmetatable(data, spritesheet)
end

function spritesheet:getImageSheet()
    return self.imageSheet
end

function spritesheet:getImage(name)
    local image = display.newImage(self.imageSheet, self.spriteSheetInfo:getFrameIndex(name))

    image.setPosition = function(x, y)
        image.x = x
        image.y = y
    end

    image.tryDrag = function()
        -- image:addEventListener("touch", Utils.onDrag)
    end

    image.setAnchor = function (x, y)
        image.anchorX = x
        image.anchorY = y
    end

    return image
end

function  spritesheet:getAnimation(name)
    local animation = display.newSprite(self.imageSheet, self.spriteSheetInfo:getSequenceData())
    animation:setSequence(name)

    animation.setPosition = function(x, y)
        animation.x = x
        animation.y = y
    end

    animation.setAnchor = function(x, y)
        animation.anchorX = x
        animation.anchorY = y
    end

    animation.tryDrag = function()
        -- animation:addEventListener("touch", Utils.onDrag)
    end
    return animation
end

function spritesheet:getAnimationWith(sequenceData)
    return display.newSprite(self.imageSheet, sequenceData)
end

function spritesheet:destroy()
    for k in pairs (self) do
        self [k] = nil
    end
end

return spritesheet