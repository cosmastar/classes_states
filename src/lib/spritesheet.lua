function display.newSheetImage(spriteSheetName, name)
    local spriteSheet = require("src.lib.n_spritesheet").new(spriteSheetName)
    local image = spriteSheet:getImage(name)

    spriteSheet:destroy()

    return image
end

function  display.newSheetAnimation(spriteSheetName, name)
    local spriteSheet = require("src.lib.n_spritesheet").new(spriteSheetName)

    local animation = spriteSheet:getAnimation(name)

    spriteSheet:destroy()

    return animation
end

function display.newSheetSeqAnimation(spriteSheetName, sequenceData)
    local spriteSheet = require("src.lib.n_spritesheet").new(spriteSheetName)

    local animation = display.newSprite(spriteSheet.imageSheet, sequenceData)

    spriteSheet:destroy()

    return animation
end