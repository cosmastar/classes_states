-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:1c41c4c5b93a0786ac58c1968a243a03:1823fed82f12f24925c1c31eb07f89d0:a0270a137714f88f9fa6e93110c00f07$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--


local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
        {
            x=1,
            y=1,
            width=130,
            height=122,
        },
        {
            x=1,
            y=125,
            width=130,
            height=122,
        },
        {
            x=1,
            y=249,
            width=130,
            height=122,
        },
        {
            x=1,
            y=373,
            width=130,
            height=122,
        },
        {
            x=1,
            y=497,
            width=130,
            height=122,
        },
        {
            x=133,
            y=1,
            width=130,
            height=122,
        },
        {
            x=133,
            y=125,
            width=130,
            height=122,
        },
        {
            x=133,
            y=249,
            width=130,
            height=122,
        },
        {
            x=133,
            y=373,
            width=130,
            height=122,
        },
        {
            x=133,
            y=497,
            width=130,
            height=122,
        },
        {
            x=631,
            y=531,
            width=62,
            height=92,
        },
        {
            x=695,
            y=531,
            width=62,
            height=92,
        },
        {
            x=759,
            y=531,
            width=62,
            height=92,
        },
        {
            x=823,
            y=531,
            width=62,
            height=92,
        },
        {
            x=887,
            y=530,
            width=62,
            height=92,
        },
        {
            x=918,
            y=425,
            width=62,
            height=92,
        },
        {
            x=919,
            y=1,
            width=62,
            height=92,
        },
        {
            x=849,
            y=1,
            width=62,
            height=92,
        },
        {
            x=919,
            y=95,
            width=62,
            height=92,
        },
        {
            x=849,
            y=95,
            width=62,
            height=92,
        },
        {
            x=919,
            y=189,
            width=62,
            height=92,
        },
        {
            x=849,
            y=189,
            width=62,
            height=92,
        },
        {
            x=919,
            y=283,
            width=62,
            height=92,
        },
        {
            x=265,
            y=1,
            width=72,
            height=116,
        },
        {
            x=265,
            y=119,
            width=72,
            height=116,
        },
        {
            x=265,
            y=237,
            width=72,
            height=116,
        },
        {
            x=265,
            y=355,
            width=72,
            height=116,
        },
        {
            x=265,
            y=473,
            width=72,
            height=116,
        },
        {
            x=339,
            y=1,
            width=72,
            height=116,
        },
        {
            x=339,
            y=119,
            width=72,
            height=116,
        },
        {
            x=339,
            y=237,
            width=72,
            height=116,
        },
        {
            x=339,
            y=355,
            width=72,
            height=116,
        },
        {
            x=339,
            y=473,
            width=72,
            height=116,
        },
        {
            x=413,
            y=1,
            width=72,
            height=116,
        },
        {
            x=413,
            y=119,
            width=72,
            height=116,
        },
        {
            x=413,
            y=237,
            width=72,
            height=116,
        },
        {
            x=413,
            y=355,
            width=72,
            height=116,
        },
        {
            x=413,
            y=473,
            width=72,
            height=116,
        },
        {
            x=487,
            y=1,
            width=72,
            height=116,
        },
        {
            x=487,
            y=119,
            width=72,
            height=116,
        },
        {
            x=487,
            y=237,
            width=72,
            height=116,
        },
        {
            x=487,
            y=355,
            width=70,
            height=104,
        },
        {
            x=487,
            y=461,
            width=70,
            height=104,
        },
        {
            x=559,
            y=355,
            width=70,
            height=104,
        },
        {
            x=559,
            y=461,
            width=70,
            height=104,
        },
        {
            x=561,
            y=1,
            width=70,
            height=104,
        },
        {
            x=561,
            y=107,
            width=70,
            height=104,
        },
        {
            x=561,
            y=213,
            width=70,
            height=104,
        },
        {
            x=631,
            y=319,
            width=70,
            height=104,
        },
        {
            x=631,
            y=425,
            width=70,
            height=104,
        },
        {
            x=633,
            y=1,
            width=70,
            height=104,
        },
        {
            x=847,
            y=425,
            width=69,
            height=103,
        },
        {
            x=633,
            y=107,
            width=70,
            height=104,
        },
        {
            x=633,
            y=213,
            width=70,
            height=104,
        },
        {
            x=703,
            y=319,
            width=70,
            height=104,
        },
        {
            x=703,
            y=425,
            width=70,
            height=104,
        },
        {
            x=705,
            y=1,
            width=70,
            height=104,
        },
        {
            x=705,
            y=107,
            width=70,
            height=104,
        },
        {
            x=705,
            y=213,
            width=70,
            height=104,
        },
        {
            x=775,
            y=319,
            width=70,
            height=104,
        },
        {
            x=775,
            y=425,
            width=70,
            height=104,
        },
        {
            x=777,
            y=1,
            width=70,
            height=104,
        },
        {
            x=777,
            y=107,
            width=70,
            height=104,
        },
        {
            x=777,
            y=213,
            width=70,
            height=104,
        },
        {
            x=847,
            y=319,
            width=70,
            height=104,
        },
    },

    sheetContentWidth = 982,
    sheetContentHeight = 624
}

SheetInfo.frameIndex =
{
    ["Dead/PersDead01"] = 1,
    ["Dead/PersDead02"] = 2,
    ["Dead/PersDead03"] = 3,
    ["Dead/PersDead04"] = 4,
    ["Dead/PersDead05"] = 5,
    ["Dead/PersDead06"] = 6,
    ["Dead/PersDead07"] = 7,
    ["Dead/PersDead08"] = 8,
    ["Dead/PersDead09"] = 9,
    ["Dead/PersDead10"] = 10,
    ["Idle/PersIdle01"] = 11,
    ["Idle/PersIdle02"] = 12,
    ["Idle/PersIdle03"] = 13,
    ["Idle/PersIdle04"] = 14,
    ["Idle/PersIdle05"] = 15,
    ["Idle/PersIdle06"] = 16,
    ["Idle/PersIdle07"] = 17,
    ["Idle/PersIdle08"] = 18,
    ["Idle/PersIdle09"] = 19,
    ["Idle/PersIdle10"] = 20,
    ["Idle/PersIdle11"] = 21,
    ["Idle/PersIdle12"] = 22,
    ["Idle/PersIdle13"] = 23,
    ["Jump/PersJump01"] = 24,
    ["Jump/PersJump02"] = 25,
    ["Jump/PersJump03"] = 26,
    ["Jump/PersJump04"] = 27,
    ["Jump/PersJump05"] = 28,
    ["Jump/PersJump06"] = 29,
    ["Jump/PersJump07"] = 30,
    ["Jump/PersJump08"] = 31,
    ["Jump/PersJump09"] = 32,
    ["Jump/PersJump10"] = 33,
    ["Jump/PersJump11"] = 34,
    ["Jump/PersJump12"] = 35,
    ["Jump/PersJump13"] = 36,
    ["Jump/PersJump14"] = 37,
    ["Jump/PersJump15"] = 38,
    ["Jump/PersJump16"] = 39,
    ["Jump/PersJump17"] = 40,
    ["Jump/PersJump18"] = 41,
    ["Run/PersOneRun01"] = 42,
    ["Run/PersOneRun02"] = 43,
    ["Run/PersOneRun03"] = 44,
    ["Run/PersOneRun04"] = 45,
    ["Run/PersOneRun05"] = 46,
    ["Run/PersOneRun06"] = 47,
    ["Run/PersOneRun07"] = 48,
    ["Run/PersOneRun08"] = 49,
    ["Run/PersOneRun09"] = 50,
    ["Run/PersOneRun10"] = 51,
    ["Run/PersOneRun11"] = 52,
    ["Run/PersOneRun12"] = 53,
    ["Run/PersOneRun13"] = 54,
    ["Run/PersOneRun14"] = 55,
    ["Run/PersOneRun15"] = 56,
    ["Run/PersOneRun16"] = 57,
    ["Run/PersOneRun17"] = 58,
    ["Run/PersOneRun18"] = 59,
    ["Run/PersOneRun19"] = 60,
    ["Run/PersOneRun20"] = 61,
    ["Run/PersOneRun21"] = 62,
    ["Run/PersOneRun22"] = 63,
    ["Run/PersOneRun23"] = 64,
    ["Run/PersOneRun24"] = 65,
}

SheetInfo.sequenceData =
{
    {   name='Dead',
        frames={  1,  2,  3,  4,  5,  6,  7,  8,  9,  10, },
        time=120, loopCount = 1
    },
    {   name='Idle',
        frames={  11,  12,  13,  14,  15,  16,  17,  18,  19,  20,  21,  22,  23, },
        time=120, loopCount = 1
    },
    {   name='Jump',
        frames={  24,  25,  26,  27,  28,  29,  30,  31,  32,  33,  34,  35,  36,  37,  38,  39,  40,  41, },
        time=120, loopCount = 1
    },
    {   name='Run',
        -- frames={  42,  43,  44,  45,  46,  47,  48,  49,  50,  51,  52,  53,  54,  55,  56,  57,  58,  59,  60,  61,  62,  63,  64,  65, },
        frames={  46,  47,  48,  49,  50,  51,  52,  53,  54,  55,  56,  57,  58,  59,  60,  61,  62,  63,  64,  65, 42,  43,  44,  45},
        time=120, loopCount = 1,
    }
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

function SheetInfo:getSequenceData()
    return self.sequenceData;
end

return SheetInfo