-----------------------------------------------------------------------------------------
--  Шаблон состояний v 0.2
--  1) Создать состояние (state.lua)
--  2) Состоянию добавить возможные переходы (transitions)
--  3) Переопределить функции onEnter() и onExit() состояния
--  4) Создать контроллер состояний (stateController.lua)
--  5) Вызвать setState(), чтобы изменить состояние
--  2017
-----------------------------------------------------------------------------------------
require("src.lib.spritesheet")

local cAnimation = require("src.classes.animation")

local animationX = display.contentCenterX - 200
local animationY = display.contentCenterY
local animation = cAnimation.new("animations", "Idle", animationX, animationY, 0.15)

local cBaseState = require("src.classes.state")
local cStateController = require("src.classes.stateController")

local stateIdle = cBaseState.new("stateIdle")
local stateRun = cBaseState.new("stateRun")

stateIdle.onEnter = function() animation:changeAnimation("Idle") animation:play() end
stateIdle.onUpdate = function() print("Idle") end
stateRun.onEnter = function() animation:changeAnimation("Run") animation.obj.y = animation.obj.y + 2 end
stateRun.onExit = function() animation.obj.y = animation.obj.y - 2 end
stateRun.onUpdate = function() print("stateRun") end

stateIdle:addTransitions(stateIdle)
stateIdle:addTransitions(stateRun)
stateRun:addTransitions(stateIdle)

local stateController = cStateController.new(stateIdle)

local timerid = timer.performWithDelay(2000, function()
    stateController:setState(stateRun)

    timer.performWithDelay(2000, function()
        stateController:setState(stateIdle)
        stateController:destroy()
    end)
end)

